FROM node:14-alpine3.11

COPY source /source

WORKDIR /source

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python

RUN npm install

CMD ["npm", "run", "start", "--", "--host", "0.0.0.0"]